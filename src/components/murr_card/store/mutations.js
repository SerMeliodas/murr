import * as type from "./type.js";

export default {
  [type.MURR_CARD_SET]: (state, payload) => (state.murr = payload),
  [type.MURR_CARD_CLEAR]: (state) => (state.murr = null),
};
